const { Router } = require('utilities');

const router = new Router('/public');
router.get({
  controller: (req) => {
    return {
      status: 'success',
      data: {
        message: 'Hello World',
      },
      cookies: [
        {
          key: 'test',
          value: '123456',
        },
        {
          key: 'test2',
          value: '654321',
          options: {
            maxAge: 60,
          },
        },
      ],
    };
  },
  path: '',
});
router.get({
  controller: (req) => {
    const { id } = req.params;
    return {
      status: 'success',
      rawData: `Your id is ${id}`,
    };
  },
  path: '/{id}',
});
router.post({
  controller: (req) => {
    return {
      status: 'success',
      data: {
        ...req.body,
      },
    };
  },
  path: '',
});
router.put({
  path: '/long-task',
  controller: (req) => {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve({
          status: 'success',
          data: {
            ...req.body,
          },
        });
      }, 5000);
    });
  },
});

module.exports = router;
