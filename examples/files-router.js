const { Router } = require('utilities');
const fileRouter = new Router('/files');
fileRouter.post({
  path: '/upload',
  controller: (req) => {
    return new Promise((resolve) => {
      req.rawHTTPRequest.busboy.on('file', (name, file, info) => {
        file.on('data', (chunk) => {
          console.log('fileName ', name);
          console.log(chunk.toString());
        });
      });
      req.rawHTTPRequest.busboy.on('error', () => {
        resolve({
          status: 'error',
          statusCode: 500,
        });
      });
      req.rawHTTPRequest.busboy.on('close', () => {
        resolve({
          status: 'success',
          rawData: 'File uploaded',
        });
      });
      req.rawHTTPRequest.pipe(req.rawHTTPRequest.busboy);
    });
  },
});
fileRouter.get({
  path: '/download',
  controller: (req) => {
    const { createReadStream } = require('fs');
    const fileStream = createReadStream('./demo.txt');
    return {
      status: 'success',
      rawData: fileStream,
      headers: {
        'Content-Type': 'text/plain',
        'Content-Disposition': 'attachment; filename=demo.txt',
      },
    };
  },
});
module.exports = fileRouter;