const Server = require('../lib');
const { Router, createLogger } = require('utilities');

const logger = createLogger({
  console: {
    enabled: true,
  },
});

const publicRouter = require('./public-router');
const fileRouter = require('./files-router');
const authRouter = require('./auth-router');
const postsRouter = require('./posts-router');

publicRouter.addChildRouter(fileRouter);

authRouter.addChildRouter(postsRouter);

const baseRouter = new Router('');
baseRouter.addChildRouter(publicRouter);
baseRouter.addChildRouter(authRouter);
const server = new Server({
  logger,
  serviceName: 'my-service',
  secretFields: {
    logging: ['password'],
  },
});
server.start(baseRouter);
