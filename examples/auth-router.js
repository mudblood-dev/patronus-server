const { Router } = require('utilities');

const users = [
  {
    id: 1,
    name: 'John',
    age: 20,
    password: '12345678',
  },
  {
    id: 2,
    name: 'Jane',
    age: 30,
    password: 'password',
  },
];

const router = new Router('/auth', (req) => {
  const userId = req.headers['userid'];
  const password = req.headers['password'];
  const user = users.find((user) => user.id === parseInt(userId, 10));
  if (!user) {
    return {
      isSuccess: false,
      statusCode: 401,
      message: 'Invalid user',
    };
  }
  if (user.password !== password) {
    return {
      isSuccess: false,
      statusCode: 401,
      message: 'Invalid password',
    };
  }
  return {
    isSuccess: true,
    authData: {
      user,
    },
  };
});

router.get({
  path: '/users',
  controller: (req) => {
    return {
      data: { users: users.map(({ password, ...user }) => user) },
    };
  },
});
router.get({
  path: '/users/current',
  controller: (req) => {
    return {
      data: { user: req.authData.user },
      statusCode: 200,
    };
  },
});

router.get({
  path: '/users/{id}',
  controller: (req) => {
    const { id } = req.params;
    const user = users.find((user) => user.id === parseInt(id, 10));
    if (!user) {
      return {
        statusCode: 404,
        message: 'User not found',
      };
    }
    return {
      data: { user },
      statusCode: 200,
    };
  },
});

module.exports = router;
