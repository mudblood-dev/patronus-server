import { Server as HttpServer } from 'http';
import { Logger, Router } from 'utilities';

export interface ServerConfig {
  logger: Logger;
  serviceName: string;
  secretFields?: {
    logging: string[];
    response: string[];
  };
  maxFileSize?: number;
}

export default class Server {
  constructor(config: ServerConfig);
  start(router: Router): HttpServer;
}
