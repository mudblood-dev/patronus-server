const http = require('http');
const { v4: getUUId } = require('uuid');
const url = require('url');
const { Stream } = require('stream');
const { matchRoutePath, validateRoutePath } = require('./utils');
const busboy = require('busboy');
const signals = [
  'SIGINT',
  'SIGTERM',
  'SIGHUP',
  'SIGQUIT',
  'SIGILL',
  'uncaughtException',
  'unhandledRejection',
];

module.exports = class Server {
  #routes = {
    get: [],
    post: [],
    put: [],
    patch: [],
    delete: [],
  };
  #isClosing = false;
  constructor({ logger, serviceName, secretFields, maxFileSize }) {
    this.logger = logger;
    this.serviceName = serviceName;
    if (secretFields) {
      this.secretFieldsForLogging = secretFields.logging;
      this.secretFieldsForResponse = secretFields.response;
    }
    this.maxFileSize = maxFileSize;
    this.currentConnections = 0;
  }

  start(router) {
    const server = this.#getHTTPServer();
    router.get({
      omitBasePath: true,
      path: '_health_',
      controller: () => {
        return {
          statusCode: 200,
          rawData: this.currentConnections - 1,
        };
      },
    });
    router.get({
      omitBasePath: true,
      path: '_routes_',
      controller: () => {
        let routes = '';
        for (const method of Object.keys(this.#routes)) {
          const methodRoutes = this.#routes[method];
          for (const { path } of methodRoutes) {
            routes += `${method.toUpperCase()} ${path}\n`;
          }
        }
        return {
          statusCode: 200,
          rawData: routes,
        };
      },
    });

    const routes = router.getAllRegisteredRoutes();
    for (const method of Object.keys(routes)) {
      const methodRoutes = routes[method];
      for (const [path, handler] of methodRoutes.entries()) {
        if (validateRoutePath(path)) {
          this.#routes[method].push({
            path: `/${this.serviceName}${path}`,
            handler,
          });
        } else {
          this.logger.error(`Invalid route path: ${path}`);
        }
      }
    }

    signals.forEach((signal) => {
      process.on(signal, (e) => {
        console.log(e);
        this.#handleSignal(signal, server);
      });
    });

    const port = process.env.SERVER_PORT || 8080;
    server.listen(port, () => {
      this.logger.info(`Server listening on port ${port}`);
    });
    return server;
  }

  /**
   *
   * @param {string} signal
   * @param {http.Server} server
   * @returns
   */
  #handleSignal(signal, server) {
    if (this.#isClosing) {
      return;
    }
    this.logger.info(`Received signal ${signal}`);
    this.#isClosing = true;
    server.close((err) => {
      if (err) process.exit(1);
      this.logger.info('Server closed');
    });
    if (this.currentConnections > 0) {
      setTimeout(() => {
        process.exit(1);
      }, 10000);
    }
  }

  #getHTTPServer() {
    return http.createServer(async (req, res) => {
      this.currentConnections++;
      req.startTime = Date.now();
      req.serviceName = this.serviceName;
      req.logger = this.logger;
      req.id = getUUId();

      this.#handleRequestEvents(req, res);
      await this.#processRequest(req, res);
      this.currentConnections--;
    });
  }

  /**
   *
   * @param {http.IncomingMessage} req
   * @param {http.OutgoingMessage} res
   * @returns
   */
  async #processRequest(req, res) {
    let statusCode = 405;
    switch (req.method) {
      case 'GET':
      case 'DELETE': {
        statusCode = await this.#handleRequestsWithoutBody(req, res);
        break;
      }
      case 'POST':
      case 'PUT':
      case 'PATCH': {
        statusCode = await this.#handleRequestsWithBody(req, res);
        break;
      }
      default: {
        res.writeHead(statusCode);
        res.end();
      }
    }
    if (!req.url.includes('/_health_')) {
      const requestInfo = this.#getRequestInfo(req);
      requestInfo.statusCode = statusCode;
      this.logger.info(JSON.stringify(requestInfo));
    }
  }

  /**
   *
   * @param {http.IncomingMessage} req
   * @returns
   */
  #createCustomRequest(req) {
    return {
      params: req.params,
      query: req.query,
      headers: req.headers,
      logger: this.logger,
      cookies: this.#parseCookie(req),
      body: req.body,
      info: {
        method: req.method,
        url: req.url,
        ip: req.socket.remoteAddress,
        host: req.headers.host,
      },
      rawHTTPRequest: req,
    };
  }
  #parseCookie(req) {
    const cookies = req.headers.cookie;
    if (!cookies) {
      return {};
    }
    return cookies
      .split(';')
      .map((cookie) => cookie.trim())
      .reduce((acc, cookie) => {
        const [key, value] = cookie.split('=');
        acc[key] = decodeURIComponent(value);
        return acc;
      }, {});
  }

  #getRoutehandler(method, reqURL) {
    for (const { path, handler } of this.#routes[method]) {
      const parsedUrl = url.parse(reqURL, true);

      const reqPath = parsedUrl.pathname;
      const query = this.#getQueryParams(parsedUrl.query);

      const params = matchRoutePath(path, reqPath);
      if (!params) continue;
      return { params, query, handler };
    }
  }

  /**
   *
   * @param {http.IncomingMessage} req
   * @param {http.OutgoingMessage} res
   * @returns {Promise<number>}
   */
  async #handleRequestsWithBody(req, res) {
    const matchedHandler = this.#getRoutehandler(
      req.method.toLowerCase(),
      req.url
    );
    if (!matchedHandler) {
      res.writeHead(404);
      res.write('Route not found');
      res.end();
      return 404;
    }
    const { params, query, handler } = matchedHandler;
    req.params = params;
    req.query = query;

    try {
      await this.#handleRequestBody(req);
    } catch (error) {
      this.logger.error(error);
      let statusCode = 500;
      if (typeof error.statusCode === 'number') {
        statusCode = error.statusCode;
        res.writeHead(statusCode);
        res.end(error.message);
      } else {
        res.writeHead(statusCode);
        res.end();
      }
      return statusCode;
    }

    const {
      statusCode = 200,
      body,
      headers,
      cookies,
    } = await handler(this.#createCustomRequest(req));

    const responseInfo = this.#handleResponseBody(body);
    if (!responseInfo) {
      res.writeHead(statusCode);
      res.end();
      return statusCode;
    }
    res.setHeader('Content-Type', responseInfo.contentType);
    if (typeof headers === 'object') {
      for (const [key, value] of Object.entries(headers)) {
        res.setHeader(key, value);
      }
    }
    if (Array.isArray(cookies)) {
      for (const cookie of cookies) {
        this.#addCookieToResponse(res, cookie);
      }
    }
    res.writeHead(statusCode);
    if (responseInfo.isStream) {
      res.pipe(responseInfo.body);
    } else {
      res.end(responseInfo.body);
    }
    return statusCode;
  }

  #addCookieToResponse(res, cookieOptions) {
    const { key, value, options } = cookieOptions;
    let cookieString = `${key}=${value}`;

    if (options) {
      if (options.domain) cookieString += `; Domain=${options.domain}`;
      if (options.path) cookieString += `; Path=${options.path}`;
      if (options.expiresAt)
        cookieString += `; Expires=${options.expiresAt.toUTCString()}`;
      if (options.maxAge) cookieString += `; Max-Age=${options.maxAge}`;
      if (options.httpOnly) cookieString += '; HttpOnly';
      if (options.secure) cookieString += '; Secure';
      if (options.sameSite) cookieString += `; SameSite=${options.sameSite}`;
    }

    res.setHeader('Set-Cookie', cookieString);
  }

  /**
   *
   * @param {http.IncomingMessage} req
   * @returns
   */
  async #handleRequestBody(req) {
    const contentType = req.headers['content-type'];
    if (!contentType) {
      req.body = await this.#processTextRequest(req);
      return;
    }
    if (
      contentType.includes('application/json') ||
      contentType.includes('text/json')
    ) {
      // JSON data
      req.body = await this.#processTextRequest(req, true);
    } else if (
      contentType.includes('multipart/form-data') ||
      contentType.includes('application/x-www-form-urlencoded')
    ) {
      // Form data
      this.#processFormDataRequest(req);
    } else if (contentType.includes('application/octet-stream')) {
      // Binary data. Nothing to do.
    } else if (
      contentType.includes('text/plain') ||
      contentType.includes('text/html')
    ) {
      req.body = await this.#processTextRequest(req);
    } else {
      // Unknown content type
      const error = new Error(`Unsupported content type: ${contentType}`);
      error.statusCode = 415;
      throw error;
    }
  }

  /**
   *
   * @param {http.IncomingMessage} req
   * @returns
   */
  #processFormDataRequest(req) {
    const bbConfig = {
      headers: req.headers,
      limits: {},
    };
    if (typeof this.maxFileSize === 'number' && this.maxFileSize > 0) {
      bbConfig.limits.fileSize = this.maxFileSize;
    }
    const bb = busboy(bbConfig);
    req.busboy = bb;
  }

  /**
   *
   * @param {http.IncomingMessage} req
   * @returns
   */
  #processTextRequest(req, isJSON = false) {
    return new Promise((resolve, reject) => {
      const data = [];
      req.on('data', (chunk) => {
        data.push(chunk);
      });
      req.on('end', () => {
        let body = Buffer.concat(data).toString();
        try {
          if (isJSON) body = JSON.parse(body);
        } catch (e) {
          const error = new Error('Invalid JSON');
          error.statusCode = 400;
          return reject(error);
        }
        resolve(body);
      });
    });
  }

  /**
   *
   * @param {http.IncomingMessage} req
   * @param {http.OutgoingMessage} res
   * @returns {Promise<number>}
   */
  async #handleRequestsWithoutBody(req, res) {
    const matchedHandler = this.#getRoutehandler(
      req.method.toLowerCase(),
      req.url
    );
    if (!matchedHandler) {
      res.writeHead(404);
      res.write('Route not found');
      res.end();
      return 404;
    }
    const { params, query, handler } = matchedHandler;
    req.params = params;
    req.query = query;
    const {
      statusCode = 200,
      body,
      headers,
      cookies,
    } = await handler(this.#createCustomRequest(req));
    const responseInfo = this.#handleResponseBody(body);
    if (!responseInfo) {
      res.writeHead(statusCode);
      res.end();
      return statusCode;
    }
    res.setHeader('Content-Type', responseInfo.contentType);
    if (typeof headers === 'object') {
      for (const [key, value] of Object.entries(headers)) {
        res.setHeader(key, value);
      }
    }
    if (Array.isArray(cookies)) {
      for (const cookie of cookies) {
        this.#addCookieToResponse(res, cookie);
      }
    }
    res.writeHead(statusCode);
    if (responseInfo.isStream) {
      responseInfo.body.pipe(res);
    } else {
      res.end(responseInfo.body);
    }
    return statusCode;
  }

  #handleResponseBody(body) {
    if (typeof body === 'undefined' || body === null) return;
    let contentType = 'application/json';
    let isStream = false;
    if (typeof body === 'object') {
      if (body instanceof Buffer) {
        contentType = 'application/octet-stream';
      } else if (body instanceof Stream) {
        isStream = true;
      } else {
        function deleteKeysFromObject(obj, keysToBeDeleted) {
          const keysSet = new Set(keysToBeDeleted);
          function recursiveDelete(obj) {
            if (typeof obj !== 'object' || obj === null) {
              return obj;
            }
            if (Array.isArray(obj)) {
              return obj.map((item) => recursiveDelete(item));
            }
            const newObj = {};
            for (const key in obj) {
              if (!keysSet.has(key)) {
                newObj[key] = recursiveDelete(obj[key]);
              }
            }
            return newObj;
          }

          return recursiveDelete(obj);
        }
        body = JSON.stringify(
          deleteKeysFromObject(body, this.secretFieldsForResponse)
        );
      }
    } else if (typeof body === 'string' || typeof body === 'number') {
      contentType = 'text/plain';
      body = body.toString();
    }
    return {
      contentType,
      body,
      isStream,
    };
  }

  #getQueryParams(rawQuery) {
    const query = {};
    for (const [key, value] of Object.entries(rawQuery)) {
      query[key] = value;
    }
    return query;
  }

  /**
   *
   * @param {http.IncomingMessage} req
   */
  #getRequestInfo(req) {
    const requestInfo = {};
    requestInfo.method = req.method;
    requestInfo.url = req.url;
    requestInfo.headers = {};
    requestInfo.id = req.id;
    requestInfo.responseTimeMS = Date.now() - req.startTime;
    for (const [key, value] of Object.entries(req.headers)) {
      if (!this.secretFieldsForLogging?.includes(key))
        requestInfo.headers[key] = value;
      else requestInfo.headers[key] = '******';
    }
    requestInfo.input = {
      query: {},
      params: {},
      body: null,
    };
    if (typeof req.query === 'object') {
      for (const [key, value] of Object.entries(req.query)) {
        if (!this.secretFieldsForLogging?.includes(key))
          requestInfo.input.query[key] = value;
        else requestInfo.input.query[key] = '******';
      }
    }
    if (typeof req.params === 'object') {
      for (const [key, value] of Object.entries(req.params)) {
        if (!this.secretFieldsForLogging?.includes(key))
          requestInfo.input.params[key] = value;
        else requestInfo.input.params[key] = '******';
      }
    }
    switch (typeof req.body) {
      case 'string':
      case 'number':
      case 'boolean': {
        requestInfo.input.body = req.body;
        break;
      }
      case 'object': {
        requestInfo.input.body = {};
        for (const [key, value] of Object.entries(req.body)) {
          if (!this.secretFieldsForLogging?.includes(key))
            requestInfo.input.body[key] = value;
          else requestInfo.input.body[key] = '******';
        }
        break;
      }
      default: {
        requestInfo.input.body = null;
      }
    }
    return requestInfo;
  }

  /**
   *
   * @param {http.IncomingMessage} req
   * @param {http.OutgoingMessage} res
   */
  #handleRequestEvents(req, res) {
    req.socket.on('end', () => {
      req.logger.info(`Connection aborted by client. Request ID: ${req.id}`);
      this.currentConnections--;
    });
    req.on('error', (err) => {
      req.logger.info(this.#getRequestInfo(req));
      req.logger.error(err);
      this.currentConnections--;
    });
  }
};
