function validateRoutePath(regPath) {
  let isPathParam = false;
  let paramLength;
  for (let i = 0; i < regPath.length; i++) {
    // disable space
    if (regPath[i] === ' ') return false;

    if (isPathParam && regPath[i] !== '}') paramLength++;

    if (regPath[i] === '{') {
      if (isPathParam) return false;
      isPathParam = true;
      paramLength = 0;
      continue;
    }
    if (regPath[i] === '}') {
      if (!isPathParam || !paramLength) return false;
      isPathParam = false;
      paramLength = 0;
    }
  }
  return !isPathParam;
}
function matchRoutePath(regPath, reqPath) {
  let regPathIdx = 0;
  let reqPathIdx = 0;
  const params = {};
  while (regPathIdx < regPath.length && reqPathIdx < reqPath.length) {
    if (regPath[regPathIdx] === reqPath[reqPathIdx]) {
      regPathIdx++;
      reqPathIdx++;
    } else if (regPath[regPathIdx] === '{') {
      regPathIdx++;
      let paramKey = '';
      while (regPath[regPathIdx] !== '}' && regPathIdx < regPath.length) {
        paramKey += regPath[regPathIdx];
        regPathIdx++;
      }
      regPathIdx++;
      let paramValue = '';
      while (reqPath[reqPathIdx] !== '/' && reqPathIdx < reqPath.length) {
        paramValue += reqPath[reqPathIdx];
        reqPathIdx++;
      }
      params[paramKey] = paramValue;
    } else {
      return false;
    }
  }
  if (regPathIdx !== regPath.length || reqPathIdx !== reqPath.length)
    return false;
  return params;
}

module.exports = {
  validateRoutePath,
  matchRoutePath,
};
